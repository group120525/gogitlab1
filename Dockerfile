# Dockerfile
FROM golang:1.22.4-bookworm

WORKDIR /app

# Copy go.mod and go.sum files to the working directory
COPY go.mod  ./

# Download all dependencies
RUN go mod download

# Copy the source code into the container
COPY . .

# Build the Go app
RUN go build -o helloworld-api .

# Expose port 8080 to the outside world
EXPOSE 8080

# Command to run the executable
CMD ["./helloworld-api"]
